package monsterhack.sdhackathon;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class Pdfview extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdfview);
        WebView mWebView=new WebView(Pdfview.this);
        mWebView.getSettings().setJavaScriptEnabled(true);
        //mWebView.getSettings().setPluginsEnabled(true);
        mWebView.loadUrl("https://docs.google.com/document/d/1xwUYzRvBlkZ6E-jfEfDOrNDj-yshwMXS6uB2cg4UJCM/edit");
        setContentView(mWebView);
    }
}
