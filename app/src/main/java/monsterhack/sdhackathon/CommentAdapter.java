package monsterhack.sdhackathon;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by james on 2016/10/2.
 */

public class CommentAdapter extends
        RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    private List<Comment> comments;
    // Store the context for easy access
    private Context context;

    // Pass in the contact array into the constructor
    public CommentAdapter(Context context, ArrayList<Comment> comments) {
        this.comments = comments;
        this.context = context;
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return context;
    }

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public ImageView profileView;
        public TextView nameView;
        public TextView timestampView;
        public TextView commentView;
        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            profileView = (ImageView) itemView.findViewById(R.id.comment_profile);
            nameView = (TextView) itemView.findViewById(R.id.comment_name);
            timestampView = (TextView) itemView.findViewById(R.id.comment_timestamp);
            commentView = (TextView) itemView.findViewById(R.id.comment_content);
            // ... view holder defined above...
            // Store a member variable for the contacts

        }

    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public CommentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.comment, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(CommentAdapter.ViewHolder viewHolder, int position) {
        // Get the data model based on position
        Comment comment = comments.get(position);

        // Set item views based on your views and data model
        viewHolder.commentView.setText(comment.getComment());
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return comments.size();
    }


}
