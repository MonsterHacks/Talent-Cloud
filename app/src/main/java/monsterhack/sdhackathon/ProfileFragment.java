package monsterhack.sdhackathon;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Locale;

public class ProfileFragment extends Fragment implements
        TextWatcher, View.OnClickListener, RadioGroup.OnCheckedChangeListener, ValueEventListener {

    public static final String TAG = "ProfileFragment";
    public static final String ARG_USER = "ProfileArgUser";

    private String uid;
    private User user;
    private DatabaseReference userRef;

    private ImageView profileView;
    private EditText emailView;
    private EditText phoneView;
    private EditText addressView;
    private EditText schoolView;
    private EditText workView;
    private EditText ageView;
    private RadioGroup genderView;
    private FloatingActionButton confirmButton;

    public ProfileFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        profileView = (ImageView) view.findViewById(R.id.profile_profile);
        emailView = (EditText) view.findViewById(R.id.profile_email);
        phoneView = (EditText) view.findViewById(R.id.profile_phone);
        addressView = (EditText) view.findViewById(R.id.profile_address);
        schoolView = (EditText) view.findViewById(R.id.profile_school);
        workView = (EditText) view.findViewById(R.id.profile_work);
        ageView = (EditText) view.findViewById(R.id.profile_age);
        genderView = (RadioGroup) view.findViewById(R.id.profile_gender);
        confirmButton = (FloatingActionButton) view.findViewById(R.id.profile_confirm);

        uid = getArguments().getString(ARG_USER);
        userRef = FirebaseDatabase.getInstance().getReference(getString(R.string.database_users));
        userRef = userRef.child(uid);

        genderView.setOnCheckedChangeListener(this);
        confirmButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        userRef.addValueEventListener(this);

        emailView.addTextChangedListener(this);
        phoneView.addTextChangedListener(this);
        addressView.addTextChangedListener(this);
        schoolView.addTextChangedListener(this);
        workView.addTextChangedListener(this);
        ageView.addTextChangedListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();

        userRef.addValueEventListener(this);

        emailView.removeTextChangedListener(this);
        phoneView.removeTextChangedListener(this);
        addressView.removeTextChangedListener(this);
        schoolView.removeTextChangedListener(this);
        workView.removeTextChangedListener(this);
        ageView.removeTextChangedListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        confirmButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        if (v == confirmButton) {

            user.setEmail(emailView.getText().toString());
            user.setPhone(phoneView.getText().toString());
            user.setAddress(addressView.getText().toString());
            user.setSchool(schoolView.getText().toString());
            user.setWork(workView.getText().toString());
            user.setAge(Long.valueOf(ageView.getText().toString()));
            switch (genderView.getCheckedRadioButtonId()) {
                case R.id.profile_gender_other:
                    user.setGender(User.GENDER_OTHER);
                    break;

                case R.id.profile_gender_female:
                    user.setGender(User.GENDER_FEMALE);
                    break;

                case R.id.profile_gender_male:
                    user.setGender(User.GENDER_MALE);
                    break;
            }

            userRef.setValue(user);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        confirmButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        user = dataSnapshot.getValue(User.class);

        emailView.setText(user.getEmail());
        phoneView.setText(user.getPhone());
        addressView.setText(user.getAddress());
        schoolView.setText(user.getSchool());
        workView.setText(user.getWork());
        ageView.setText(String.format(Locale.getDefault(), "%d", user.getAge()));
        switch (user.getGender()) {
            case User.GENDER_OTHER:
                genderView.check(R.id.profile_gender_other);
                break;

            case User.GENDER_FEMALE:
                genderView.check(R.id.profile_gender_female);
                break;

            case User.GENDER_MALE:
                genderView.check(R.id.profile_gender_male);
                break;
        }
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        Log.e(TAG, "Firebase Error", databaseError.toException());
        Toast.makeText(getContext(), "Could not connect, try later", Toast.LENGTH_SHORT).show();
    }
}
