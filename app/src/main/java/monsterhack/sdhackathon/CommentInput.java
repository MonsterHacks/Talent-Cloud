package monsterhack.sdhackathon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class CommentInput extends AppCompatActivity {

    public static final String COMMENT_STRING = "CommentInput.COMMENT_STRING";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_input);
    }

    public void saveComment(View view) {
        TextView saving = (TextView) findViewById(R.id.InputComment);
        CharSequence content = saving.getText();
        Intent data = new Intent();
        data.putExtra(COMMENT_STRING, content.toString());
        setResult(RESULT_OK, data);
        finish();
    }
}
