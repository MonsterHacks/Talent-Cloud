package monsterhack.sdhackathon;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    public static final CharSequence[] TABS = {"HOME", "PROFILE", "SETTINGS"};

    private static final int RC_COMMENT = 210;
    private static final int RC_LOOK = 211;

    private MainPagerAdapter pagerAdapter;
    private ViewPager viewPager;

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;

    private User currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

        currentUser = new User();
        currentUser.setUid(firebaseUser.getUid());
        currentUser.setName(firebaseUser.getDisplayName());
        currentUser.setProfile(firebaseUser.getPhotoUrl().toString());
        currentUser.setEmail(firebaseUser.getEmail());

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference reference = database.getReference(getString(R.string.database_users));
        DatabaseReference currUserRef = reference.child(currentUser.getUid());
        currUserRef.setValue(currentUser);

        pagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        viewPager = (ViewPager) findViewById(R.id.main_pager);
        viewPager.setAdapter(pagerAdapter);
    }

    public void Look(View view){
        Intent intent = new Intent(this, Pdfview.class);
        startActivityForResult(intent, RC_LOOK);
    }

    public void Comment(View view){
        Intent intent = new Intent(this, CommentsActivity.class);
        startActivityForResult(intent, RC_COMMENT);
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public class MainPagerAdapter extends FragmentStatePagerAdapter {

        public MainPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new PostFragment();
                case 1:
                    Fragment fragment = new ProfileFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(ProfileFragment.ARG_USER, currentUser.getUid());
                    fragment.setArguments(bundle);
                    return fragment;
                case 2:
                    return new SettingFragment();

                default:
                    return new Fragment();
            }
        }

        @Override
        public int getCount() {
            return TABS.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TABS[position];
        }
    }
}
