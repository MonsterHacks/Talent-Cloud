package monsterhack.sdhackathon;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {

    private ArrayList<Post> data;
    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView profileView;
        public TextView nameView;
        public TextView timestampView;
        public TextView contentView;
        public TextView statsView;

        public ViewHolder(View view) {
            super(view);
            profileView = (ImageView) view.findViewById(R.id.post_profile);
            nameView = (TextView) view.findViewById(R.id.post_name);
            timestampView = (TextView) view.findViewById(R.id.post_timestamp);
            contentView = (TextView) view.findViewById(R.id.post_content);
            statsView = (TextView) view.findViewById(R.id.post_stats);
        }
    }

    public PostAdapter(Context context, ArrayList<Post> data) {
        this.context = context;
        this.data = data;

        User user = new User();
        user.setName("Gunawan Makmur");
        Post post1 = new Post();
        post1.setTimestamp(new Date().getTime());
        post1.setLikes(0);
        Comment[] comments = new Comment[1];
        comments[0] = new Comment();

        post1.setComments(comments);
        post1.setShares(1);
        post1.setContent("Lorem ipsum dol a set");

        data.add(post1);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.post, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Post post = data.get(position);
        //holder.nameView.setText(post.getPoster().getName());

        Date timestamp = new Date(post.getTimestamp());
        DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(context);
        holder.timestampView.setText(dateFormat.format(timestamp));

        holder.contentView.setText(post.getContent());

        holder.statsView.setText("" + post.getLikes() + " likes " + post.getComments().length + " comments " + post.getShares() + " shares");
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
