package monsterhack.sdhackathon;

import java.util.Date;

/**
 * Created by james on 2016/10/2.
 */

public class Comment {
    private String judge;
    private long like;
    private long timestamp;
    private String userId;

    public Comment() {
        judge = "";
        like = 0;
        timestamp = new Date().getTime();
        userId = "";
    }

    public Comment (String uid) {
        judge = "";
        //judge.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        //judge.setText(void_char);
        like = 0;
        timestamp = new Date().getTime();
    }

    public Comment (String s, String uid) {
        //judge = new TextView(context);
        //judge.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        //judge.setText(s);
        judge = s;
        like = 0;
        timestamp = new Date().getTime();
    }

    public void like() {
        like++;
    }

    public long getLikes(){
        return like;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getUserId() {
        return userId;
    }

    public String getComment() {
        return judge;
    }

}
