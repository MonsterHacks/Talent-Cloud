package monsterhack.sdhackathon;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class PostFragment extends Fragment {

    private RecyclerView postView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;

    public PostFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mainView = inflater.inflate(R.layout.fragment_post, container, false);

        postView = (RecyclerView) mainView.findViewById(R.id.list_post);

        layoutManager = new LinearLayoutManager(getContext());
        postView.setLayoutManager(layoutManager);

        adapter = new PostAdapter(getContext(), new ArrayList<Post>());
        postView.setAdapter(adapter);

        return mainView;
    }

}
