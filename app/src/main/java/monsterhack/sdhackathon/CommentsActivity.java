package monsterhack.sdhackathon;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

public class CommentsActivity extends AppCompatActivity {

    private RecyclerView commentsList;
    private EditText commentsEditText;
    private RecyclerView.Adapter adapter;
    private ArrayList<Comment> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        commentsList = (RecyclerView) findViewById(R.id.comments_list);
        commentsEditText = (EditText) findViewById(R.id.comments_edittext);

        commentsList.setLayoutManager(new LinearLayoutManager(this));

        data = new ArrayList<>();
        adapter = new CommentAdapter(this, data);
        commentsList.setAdapter(adapter);
    }

    public void saveComment(View view) {
        String comment = commentsEditText.getText().toString();
        if (comment.isEmpty()) {
            return;
        }

        data.add(new Comment(comment, FirebaseAuth.getInstance().getCurrentUser().getUid()));
        commentsEditText.getText().clear();
        adapter.notifyDataSetChanged();
    }
}
