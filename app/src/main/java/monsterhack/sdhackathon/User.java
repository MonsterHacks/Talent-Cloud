package monsterhack.sdhackathon;

public class User {

    public static final String GENDER_OTHER = "other";
    public static final String GENDER_FEMALE = "female";
    public static final String GENDER_MALE = "male";

    private String uid;
    private String name;
    private String profile;
    private String email;
    private String phone;
    private String address;
    private String school;
    private String work;
    private long age;
    private String gender;

    public User() {
        uid = "";
        name = "";
        profile = "";
        email = "";
        phone = "";
        address = "";
        school = "";
        work = "";
        age = 0;
        gender = "";
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
