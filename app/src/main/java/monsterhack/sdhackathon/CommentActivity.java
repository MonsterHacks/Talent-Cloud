package monsterhack.sdhackathon;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

//import com.google.android.gms.appindexing.Action;
//import com.google.android.gms.appindexing.AppIndex;
//import com.google.android.gms.appindexing.Thing;
//import com.google.android.gms.common.api.GoogleApiClient;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

public class CommentActivity extends FragmentActivity{

    private static final int RC_COMMENT_INPUT = 451;

    ArrayList<Comment> contacts = new ArrayList<Comment>();
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    //private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // ...
        // Lookup the recyclerview in activity layout
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
/*
        String like = "like this!";
        for(int i=0; i<10; i++){
            Comment temp = new Comment(like);
            contacts.add(temp);
        }*/

        //View CommentView = inflater.inflate(R.layout.activity_comment, container, false);
        RecyclerView rvContacts = (RecyclerView) findViewById(R.id.rvContacts_1);

        //View child = getLayoutInflater().inflate(R.layout.activity_comment, null);
        //rvContacts.addView(child);

        CommentAdapter adapter = new CommentAdapter(this, contacts);

        rvContacts.setLayoutManager(new LinearLayoutManager(this));
        // Attach the adapter to the recyclerview to populate items
        rvContacts.setAdapter(adapter);
        // Set layout manager to position the items


        // Initialize contacts
        // Create adapter passing in the sample user data
        // That's all!
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        //client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    /*public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Comment Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }*/

    //@Override
   /* public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }*/

   /* @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }*/

    public void addcomment(View view) {
        Intent intent = new Intent(this, CommentInput.class);
        startActivityForResult(intent, RC_COMMENT_INPUT);
    }

    public void saveComment(View view) {
        /*TextView saving = (TextView) findViewById(R.id.InputComment);
        CharSequence content = saving.getText();
        Comment temp = new Comment (content);
        contacts.add(temp);
        RecyclerView rvContacts = (RecyclerView) findViewById(R.id.rvContacts_1);

        //View child = getLayoutInflater().inflate(R.layout.activity_comment, null);
        //rvContacts.addView(child);

        CommentAdapter adapter = new CommentAdapter(this, contacts);

        rvContacts.setLayoutManager(new LinearLayoutManager(this));
        // Attach the adapter to the recyclerview to populate items
        rvContacts.setAdapter(adapter);*/

        Intent intent = new Intent(this, CommentActivity.class);
        startActivity(intent);

        //this.getFragmentManager().beginTransaction().hide(CommentInputFragment);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RC_COMMENT_INPUT:
                if (resultCode == RESULT_OK) {
                    String comment = data.getStringExtra(CommentInput.COMMENT_STRING);
                    String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    Comment temp = new Comment(comment, uid);
                    contacts.add(temp);
                }
                break;
        }
    }
}
